import im
import srv
import atexit
import time

def main():
    print('starting...')
    im.init()
    # while(True):
    #     time.sleep(0.5)
    #     print('.')
    srv.listen()

def closing():
    print('closing...')
    im.close()

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        closing()
    

atexit.register(closing)