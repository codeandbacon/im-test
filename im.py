import os
import json

json_path = './contacts.json'
contacts = {}

def close():
    pass

def init():
    if os.path.isfile(json_path):
        with open(json_path, 'rb') as f:
            contacts = json.loads(f.read())

    return contacts